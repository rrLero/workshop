import React, { useEffect } from 'react';

import { Props } from './logout.types';

const LogoutContainer: React.FC<Props> = ({ history }: Props) => {

  useEffect(() => {
    console.log('logout user');
    history.push('/');
  }, [ history ]);

  return null;
};

export default LogoutContainer;
