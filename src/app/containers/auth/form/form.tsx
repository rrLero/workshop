import React, { ChangeEvent, FormEvent, useState } from 'react';

import { Props } from './form.types';

const FormLayout: React.FC<Props> = ({ loginUser }: Props) => {

  const [ email, setEmail ] = useState('');

  const [ password, setPassword ] = useState('');

  const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    loginUser(email, password);
  };

  const handleEmail = (event: ChangeEvent<HTMLInputElement>) => {
    setEmail(event.target.value);
  };

  const handlePassword = (event: ChangeEvent<HTMLInputElement>) => {
    setPassword(event.target.value);
  };

  return (
    <form onSubmit={handleSubmit}>
      <div className="form-group">
        <label htmlFor="exampleInputEmail1">Email address</label>
        <input
          type="email"
          className="form-control"
          id="exampleInputEmail1"
          value={email}
          onChange={handleEmail}
          aria-describedby="emailHelp"
          placeholder="Enter email"/>
        <small
          id="emailHelp"
          className="form-text text-muted">We'll never share your email with anyone else.
        </small>
      </div>
      <div className="form-group">
        <label htmlFor="exampleInputPassword1">Password</label>
        <input
          type="password"
          value={password}
          onChange={handlePassword}
          className="form-control"
          id="exampleInputPassword1"
          placeholder="Password"/>
      </div>
      <div className="form-group form-check">
        <input
          type="checkbox"
          className="form-check-input"
          id="exampleCheck1"/>
        <label
          className="form-check-label"
          htmlFor="exampleCheck1">
          Check me out
        </label>
      </div>
      <button
        type="submit"
        className="btn btn-primary">Submit
      </button>
    </form>
  );
};

export default FormLayout;
