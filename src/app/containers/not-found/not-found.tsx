import React from 'react';

import { useTranslation } from 'react-i18next';

import Styles from './not-found.module.scss';

const NotFoundContainer: React.FC = () => {

  const [ t ] = useTranslation();

  return (
    <div className={Styles.container}>
      <h1>404</h1>
      <p>{t('common.404.header')}</p>
    </div>
  );
};

export default NotFoundContainer;
