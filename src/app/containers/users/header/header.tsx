import React from 'react';

import { Link } from 'react-router-dom';

const HeaderLayout: React.FC = () => {
  return (
    <nav className="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
      <Link to="/users" className="navbar-brand col-sm-3 col-md-2 mr-0">SPD Ukraine</Link>
      <ul className="navbar-nav px-3">
        <li className="nav-item text-nowrap">
          <Link to="/auth/logout" className="nav-link">Sign out</Link>
        </li>
      </ul>
    </nav>
  );
};

export default HeaderLayout;
