import React, { useCallback, useEffect, useState } from 'react';

import { Container, Column } from 'components/table'

import Styles from './list.module.scss';
import { getUsersList } from 'src/app/rest/users/users.service';
import { UsersParams } from 'src/app/rest/users/users.params';
import { UsersListDto } from 'src/app/rest/users/users.list.dto';
import { Link } from 'react-router-dom';
import { UsersResponseDto } from 'src/app/rest/users/users.response.dto';

const UsersListContainer: React.FC = () => {

  const [ params ] = useState(new UsersParams());

  const [ usersList, setUsersList ] = useState(new UsersListDto());

  const initUsersList = useCallback(() => {
    getUsersList(params)
      .subscribe(setUsersList);
  }, [ params, setUsersList ]);

  useEffect(() => {
    initUsersList();
  }, [ initUsersList ]);

  return (
    <div className={Styles.container}>
      <Container
        items={usersList ? usersList.content : []}
        params={params}
        update={initUsersList}>
        <Column
          id="id"
          name="ID"
          width={30} />
        <Column
          id="email"
          name="Email"
          width={60} />
        <Column width={10}>
          {(data: UsersResponseDto) => (
            <span>
              <Link to={`/portal/users/${data.id}/details`} className="details">
                <i className="fas fa-info-circle"/>
              </Link>
              {data.blocked ? (
                <span className="unblock-user">
                  <i className="fas fa-lock-open"/>
                </span>
              ) : (
                <span className="block-user">
                  <i className="fas fa-lock"/>
                </span>
              )}
            </span>
          )}
        </Column>
      </Container>
    </div>
  );
};

export default UsersListContainer;
