import React from 'react';
import { Route, Switch, Redirect } from 'react-router';

import HeaderLayout from './header';

import UsersListContainer from './list';

import { Props } from './users.types';

import Styles from './users.module.scss';

const UsersContainer: React.FC<Props> = ({ history }: Props) => {
  return (
    <div className={Styles.container}>
      <HeaderLayout/>
      <Switch>
        <Route
          path="/users/list"
          component={UsersListContainer}/>
        <Redirect
          from="/users"
          to="/users/list"/>
      </Switch>
    </div>
  );
};

export default UsersContainer;
