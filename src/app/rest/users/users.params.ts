import { Params } from 'helpers/rest/rest.params';

export class UsersParams implements Params {

  page?: number;
  size?: number;
  sort?: string;

  constructor(data?: UsersParams) {
    if (data) {
      this.page = data.page;
      this.size = data.size;
      this.sort = data.sort;
    }
  }
}
