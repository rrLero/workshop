import { RestDto } from 'helpers/rest/rest.dto';

import { UsersResponseDto } from './users.response.dto';

export class UsersListDto extends RestDto<UsersResponseDto> {}
