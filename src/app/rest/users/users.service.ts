import { map, take } from 'rxjs/operators';
import { Observable } from 'rxjs/internal/Observable';

import http from 'helpers/rest/rest.service';

import { UsersParams } from './users.params';
import { UsersListDto } from './users.list.dto';

export function getUsersList(params: UsersParams): Observable<UsersListDto> {
  return http.get<UsersListDto>('/users', params)
    .pipe(
      map(data => new UsersListDto(data))
    );
}
