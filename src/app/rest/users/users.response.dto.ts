export class UsersResponseDto {

  id?: string;
  email?: string;
  blocked?: boolean;

  constructor(data?: UsersResponseDto) {
    if (data) {
      this.id = data.id;
      this.email = data.email;
      this.blocked = data.blocked;
    }
  }
}
