export class TokensResponseDto {

  expireAt: string = '';
  accessToken: string = '';

  constructor(data?: TokensResponseDto) {
    if (data) {
      this.expireAt = data.expireAt;
      this.accessToken = data.accessToken;
    }
  }
}
