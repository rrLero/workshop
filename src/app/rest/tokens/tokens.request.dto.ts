export class TokensRequestDto {

  email: string = '';
  password: string = '';

  constructor(data?: TokensRequestDto) {
    if (data) {
      this.email = data.email;
      this.password = data.password;
    }
  }
}
