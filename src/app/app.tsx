import React, { Fragment } from 'react';

import { Switch, Router } from 'react-router';

import { Route } from 'react-router-dom';

import { ToastContainer } from 'react-toastify';

import AuthContainer from 'containers/auth';
import UsersContainer from 'containers/users';
import LogoutContainer from 'containers/auth/logout';
import NotFoundContainer from 'containers/not-found';

import { initTranslation } from 'helpers/localization';

import history from 'helpers/history';

const App: React.FC = () => {
  return (
    <Fragment>
      <ToastContainer />
      <Router history={history}>
        <Switch>
          <Route
            path="/users"
            component={UsersContainer} />
          <Route
            path="/"
            exact
            component={AuthContainer} />
          <Route
            path="/auth/logout"
            exact
            component={LogoutContainer} />
          <Route
            path="**"
            component={NotFoundContainer} />
        </Switch>
      </Router>
    </Fragment>
  );
};

initTranslation();

export default App;
