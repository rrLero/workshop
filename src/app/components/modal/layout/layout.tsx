import React, { useEffect } from 'react';

import ReactDOM from 'react-dom';

import { Props } from './layout.types';

const modalRoot = document.getElementById('modal-root');

const Modal: React.FC<Props> = ({ children }) => {

  const element: HTMLElementTagNameMap['div'] = document.createElement('div');

  useEffect(() => {
    if (modalRoot) {
      modalRoot.appendChild(element);
    }
    return () => {
      if (modalRoot) {
        modalRoot.removeChild(element);
      }
    };
  }, [ element ]);

  return ReactDOM.createPortal(
    children,
    element,
  );
};

export default Modal;
