import React from 'react';

import { Props } from './confirmation.types';

import './confirmation.scss';

const ConfirmationModal: React.FC<Props> = ({ visible, name, message, buttonTitle, success, cancel }: Props) => {
  return (
    <div className={`modal fade ${visible ? 'show' : ''}`}>
     <div className="modal-dialog">
       <div className="modal-content">
         <div className="modal-header">
           <h5
             id="exampleModalLabel"
             className="modal-title">
             {name}
           </h5>
           <button
             type="button"
             data-dismiss="modal"
             aria-label="Close"
             onClick={cancel}>
             <span aria-hidden="true">&times;</span>
           </button>
         </div>
         <div className="modal-body">
           {message}
         </div>
         <div className="modal-footer">
           <button
             type="button"
             className="btn btn-secondary"
             data-dismiss="modal"
             onClick={cancel}>
             Close
           </button>
           {buttonTitle ? (
             <button
               type="button"
               className="btn btn-danger"
               onClick={success}>
               {buttonTitle}
             </button>
           ) : null}
         </div>
       </div>
     </div>
    </div>
  );
};

export default ConfirmationModal;
