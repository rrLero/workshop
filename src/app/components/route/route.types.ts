import React from 'react';

export interface Props {
  path: string;
  component: React.FC<any>;
  exact?: boolean;
  canActive?: () => boolean;
  redirectTo?: string;
}
