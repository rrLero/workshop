import React, { useCallback, useEffect, useState } from 'react';

import { useTranslation } from 'react-i18next';

import { Props } from './navigation.types';

const TableNavigation: React.FC<Props> = ({ params, totalPages, totalItems, update }: Props) => {

  const [ t ] = useTranslation();

  const [ page, setPage ] = useState<number>(1);

  const [ lastPage, setLastPage ] = useState<boolean>(false);

  const [ sections, setSections ] = useState<number[]>([]);

  const sectionCount = 6;

  const changeListPage = () => {
    params.page = page;
    update();
  };

  const openPage = (currentPage: number) =>  {
    setPage(currentPage);
    setLastPage(page === totalPages - 1);
    changeListPage();
    createSectionPagesRange(Math.floor(page / sectionCount));
  };

  const createSectionPagesRange = useCallback((section: number) => {
    const start = sectionCount * section;
    const range = new Array(totalPages).slice(start, start + sectionCount).fill(0);
    setSections(range.map((item, index) => start + index));
  }, [totalPages]);

  useEffect(() => {
    setLastPage(false);
    setPage(0);
    createSectionPagesRange(0);
  }, [totalPages, totalItems, createSectionPagesRange]);

  return (
    <nav className="table-navigation">
      {totalPages > 1 ? (
        <ul className="pagination">
          <li
            className={`page-item ${page === 0 ? 'disabled' : ''}`}>
            <span
              className="page-link"
              onClick={() => page !== 0 && openPage(page - 1)}>
              {t('common.table.prev')}
            </span>
          </li>
          {sections.map((section, index) => (
            <li
              key={index}
              className={`page-item ${page === section ? 'active' : ''}`}>
              <span
                className="page-link"
                onClick={() => openPage(section)}>
                {section + 1}
              </span>
            </li>
          ))}
          <li
            className={`page-item ${lastPage ? 'disabled' : ''}`}>
            <span
              className="page-link"
              onClick={() => !lastPage && openPage(page + 1)}>
              {t('common.table.next')}
            </span>
          </li>
        </ul>
      ) : null}
    </nav>
  );
};

export default TableNavigation;
