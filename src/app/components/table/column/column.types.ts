export interface Props {
  id?: string;
  name?: string;
  data?: any;
  width?: number;
  centered?: boolean;
  children?: (data?: any) => void;
}
