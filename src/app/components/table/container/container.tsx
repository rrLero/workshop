import React, { useState } from 'react';

import { useTranslation } from 'react-i18next';

import { Props } from './container.types';

import './container.scss';

const TableContainer: React.FC<Props> = ({ items = [], children = [], params, update }: Props) => {

  const [ t ] = useTranslation();

  const [ sortField, setSortField ] = useState<string>('');

  const [ sortDirection, setSortDirection ] = useState<string>('');

  const changeSort = (name: string) => {
    setSortField(name);
    setSortDirection(getSortDirection(name));
    if (params) {
      params.sort = `${name},${sortDirection}`;
      update();
    }
  };

  const getSortDirection = (field: string) => {
    const direction = sortField !== field || sortDirection === 'desc';
    return direction ? 'asc' : 'desc';
  };

  return (
    <div className="table-responsive">
      <table className="table table-striped table-sm table-hover">
        <thead>
        <tr>
          {React.Children.map(children, (column, index) => (
            <th
              key={index}
              style={{width: `${column.props.width}%`}}
              className={column.props.centered ? 'centered' : ''}
              onClick={() => column.props.id && changeSort(column.props.id)}>
              {column.props.name}
              &nbsp;
              {sortField === column.props.id ? (
                <i className={`fas ${sortDirection === 'desc' ? 'fa-sort-alpha-up' : 'fa-sort-alpha-down'}`}/>
              ) : null}
            </th>
          ))}
        </tr>
      </thead>
      <tbody>
      {(items || []).map((item, index) => (
        <tr key={index}>
          {React.Children.map(children, (column, index) => (
            <td
              key={index}
              style={{width: `${column.props.width}%`}}
              className={column.props.centered ? 'centered' : ''}>
              {column.props.children ? column.props.children(item) : null}
              {!column.props.children && column.props.id ? (
                <span>{item[column.props.id]}</span>
              ) : null}
            </td>
          ))}
        </tr>
      ))}
      {!items.length ? (
        <tr>
          <td colSpan={children.length}>
            {t('common.table.no-items')}
          </td>
        </tr>
      ) : null}
      </tbody>
    </table>
  </div>
  );
};

export default TableContainer;
