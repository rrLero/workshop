export interface Params {

  page?: number;
  size?: number;
  sort?: string;
}
