import { RestPageableDto } from './rest.pageable.dto';

export class RestDto<T> {

  content: T[] = [];
  first: boolean = false;
  last: boolean = false;
  number: number = 0;
  numberOfElements: number = 0;
  pageable = new RestPageableDto();
  size: number = 0;
  sort: string[] = [];
  totalElements: number = 0;
  totalPages: number = 0;

  constructor(data?: RestDto<T>) {
    if (data) {
      this.content = data.content;
      this.pageable = new RestPageableDto(data.pageable);
      this.first = data.first;
      this.last = data.last;
      this.number = data.number;
      this.numberOfElements = data.numberOfElements;
      this.size = data.size;
      this.sort = data.sort;
      this.totalElements = data.totalElements;
      this.totalPages = data.totalPages;
    }
  }
}
