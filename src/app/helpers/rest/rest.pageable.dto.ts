export class RestPageableDto {

  page: number = 0;
  size: number = 0;
  sort: string[] = [];

  constructor(data?: RestPageableDto) {
    if (data) {
      this.page = data.page;
      this.size = data.size;
      this.sort = data.sort;
    }
  }
}
