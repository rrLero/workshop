import { ajax, AjaxError } from 'rxjs/ajax';
import { catchError, map, take } from 'rxjs/operators';
import { throwError } from 'rxjs/internal/observable/throwError';
import { Observable } from 'rxjs/internal/Observable';

import i18n from 'i18next';

import { toast } from 'react-toastify';

import qs from 'qs';

import { getToken } from 'helpers/auth';

interface Config {
  [key: number]: string
}

class RestService {
  get<T>(url: string, params?: Object): Observable<T> {
    return ajax.get(this.parsePath(url, params), this.getHeaders())
      .pipe(
        map(data => data.response as T),
        take(1)
      );
  }

  post<T>(url: string, body: any, params?: Object): Observable<T> {
    return ajax.post(this.parsePath(url, params), body, this.getHeaders())
      .pipe(
        map(data => data.response as T),
        take(1)
      );
  }

  put<T>(url: string, body: any, params?: Object): Observable<T> {
    return ajax.put(this.parsePath(url, params), body, this.getHeaders())
      .pipe(
        map(data => data.response as T),
        take(1)
      );
  }

  delete<T>(url: string, params?: Object): Observable<T> {
    return ajax.delete(this.parsePath(url, params), this.getHeaders())
      .pipe(
        map(data => data.response as T),
        take(1)
      );
  }

  responseHandle<T>(response: Observable<T>, config: Config): Observable<T> {
    return response
      .pipe(
        map(response => this.responseSuccess<T>(response, config)),
        catchError(error => this.responseError(error, config))
      );
  }

  private responseSuccess<T>(response: T, config: Config): T {
    if (config[ 200 ]) {
      toast.success(i18n.t(config[ 200 ]));
    }
    return response;
  }

  private responseError(error: AjaxError, config: Config): Observable<never> {
    if (config[ error.status ]) {
      toast.error(i18n.t(config[ error.status ]));
    }

    return throwError('Виявлено помилку');
  }

  private getHeaders(): {[key: string]: string} {
    return {
      'Access-Token': getToken(),
      'Content-Type': 'application/json'
    }
  }

  private parsePath(url: string, params?: Object) {
    const prefix = process.env.REACT_APP_API_PREFIX as string;
    return prefix + url + (params ? `?${qs.stringify(params)}` : '');
  }
}

export default new RestService();
