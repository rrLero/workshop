import { initReactI18next } from 'react-i18next';

import i18n from 'i18next';
import Backend from 'i18next-xhr-backend';

import { get, set } from 'local-storage';

import { LanguagesEnum } from 'enums/languages.enum';

import en from 'locales/en-GB.json';
import uk from 'locales/uk-UA.json';

export function getLocalization (): string {
  return get<string>('localization') || LanguagesEnum.ENGLISH;
}

export function setLocalization (localization: string = LanguagesEnum.ENGLISH) {
  set<string>('localization', localization);
}

export function initTranslation () {
  i18n
    .use(Backend)
    .use(initReactI18next)
    .init({
      lng: getLocalization(),
      fallbackLng: LanguagesEnum.ENGLISH,
      resources: {
        en: {
          translation: en
        },
        uk: {
          translation: uk
        }
      },
      react: {
        useSuspense: false
      },
      interpolation: {
        escapeValue: false
      }
    });
}
